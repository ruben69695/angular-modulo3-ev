var express = require('express'), cors = require('cors');
var app = express();

const PORT = 3000;

app.use(express.json());
app.use(cors());
app.listen(PORT, () => console.log('Server running on port ', PORT));

var myMovies = [
    { name: 'Star Wars', description: 'This is a little description to test', date: new Date() },
    { name: 'The Avengers EndGame', description: 'This is a little description to test', date: new Date() },
    { name: 'Sherlock Holmes', description: 'This is a little description to test', date: new Date() },
    { name: 'Avatar', description: 'This is a little description to test', date: new Date() }
];

app.get('/movies', (req, res, next) => res.json(myMovies));
app.post('/movies', (req, res, next) => {
    console.log(req.body);
    myMovies.push(req.body);
    res.json(req.body);
});

app.get('/api/translations', (req, res, next) => {
    if (req.query.lang == 'fr') {
        res.json([
            { lang: req.query.lang, key: 'Saludar', value: 'Bonjour' },
            { lang: req.query.lang, key: 'Adio', value: 'Adieu' }
        ]);
    } else if (req.query.lang == 'es') {
        res.json([
            { lang: req.query.lang, key: 'Saludar', value: 'Hola' },
            { lang: req.query.lang, key: 'Adio', value: 'Adiós' }
        ])
    } else {
        res.json([
            { lang: req.query.lang, key: 'Saludar', value: 'Hello' },
            { lang: req.query.lang, key: 'Adio', value: 'Goodbye' }
        ]);
    }
});
