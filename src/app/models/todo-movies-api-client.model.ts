import { Injectable, Inject, forwardRef } from '@angular/core';
import { TodoMovieItem } from './todo-movie-item.models';
import { Store } from '@ngrx/store';
import { AppState } from '../store/reducers/main-reducer';
import { VoteUpTodoMovieAction, VoteDownTodoMovieAction, AddTodoMovieAction } from '../store/actions/movies-items.actions';
import { HttpHeaders, HttpClient, HttpRequest, HttpResponse } from '@angular/common/http';
import { APP_CONFIG, AppConfig } from '../resources/app-config';
import { NewTodoItemAction } from '../store/actions/todo-items.actions';
import { DatabaseService } from '../services/database.service';

@Injectable()
export class TodoMoviesApiClient {
    movies: TodoMovieItem[];
    constructor(private store: Store<AppState>,
                @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig,
                private http: HttpClient, private dbManager: DatabaseService) {
        this.store
            .select(state => state.movies)
            .subscribe((data) => {
                this.movies = data.items;
            });
    }
    voteUp(movie: TodoMovieItem) {
        this.store.dispatch(new VoteUpTodoMovieAction(movie));
    }
    voteDown(movie: TodoMovieItem) {
        this.store.dispatch(new VoteDownTodoMovieAction(movie));
    }
    getAll(): TodoMovieItem[] {
        return this.movies;
    }
    getById(id: string): TodoMovieItem {
        console.log('awacate');
        return new TodoMovieItem('Star Wars', new Date(), 'Little description');
    }
    add(item: TodoMovieItem): void {
        const jsonHeader: HttpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });
        const req = new HttpRequest('POST', this.config.endPoint + '/movies',
        { name: item.name, description: item.description, date: item.creationDate },
        { headers: jsonHeader });
        this.http.request(req).subscribe((data: HttpResponse<{}>) => {
            if (data.status === 200) {
                console.log(data.status);
                this.store.dispatch(new AddTodoMovieAction(item));
                this.dbManager.movies.add(item);
                this.dbManager.movies.toArray().then(movies => console.log('MyDatabase: has ', movies));
            }
        });
    }
}

