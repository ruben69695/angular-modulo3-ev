import { Injectable, InjectionToken, Inject } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../store/reducers/main-reducer';
import { HttpHeaders, HttpRequest, HttpClient } from '@angular/common/http';
import { InitDataTodoMovieAction } from '../store/actions/movies-items.actions';
import { APP_CONFIG, AppConfig } from './../resources/app-config';

export function init_app(appLoadService: AppLoadService): () => Promise<any> {
  return () => appLoadService.initializeMoviesState();
}

@Injectable({
  providedIn: 'root'
})
export class AppLoadService {
  constructor(private store: Store<AppState>, private http: HttpClient, @Inject(APP_CONFIG) private config: AppConfig) { }
  async initializeMoviesState(): Promise<any> {
    const headerContentType: HttpHeaders = new HttpHeaders({'Content-Type': 'application/json'});
    const req = new HttpRequest('GET', this.config.endPoint + '/movies', { headers: headerContentType });
    const response: any = await this.http.request(req).toPromise();
    this.store.dispatch(new InitDataTodoMovieAction(response.body));
  }
}
