import { Injectable } from '@angular/core';
import Dexie from 'dexie';
import { TodoMovieItem } from '../models/todo-movie-item.models';
import { Translation } from '../models/translation.models';

@Injectable({
  providedIn: 'root'
})
export class DatabaseService extends Dexie {
  movies: Dexie.Table<TodoMovieItem, number>;
  translations: Dexie.Table<Translation, number>;
  constructor() {
    super('MyDatabase');
    this.version(1).stores({
      movies: '++id, name, description, creationDate, votes',
    });
    this.version(2).stores({
      movies: '++id, name, description, creationDate, votes',
      translations: '++id, lang, key, value'
    });
  }
}

export const db = new DatabaseService();
