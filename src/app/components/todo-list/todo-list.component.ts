import { Component, OnInit, HostBinding } from '@angular/core';
import { TodoItem } from '../../models/todo-item.models';
import { TodoItemsApiClient } from '../../models/todo-items-api-client.model';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css'],
  providers: [ TodoItemsApiClient ]
})
export class TodoListComponent implements OnInit {

  @HostBinding('attr.class') cssClass = 'container content mt-4';

  constructor(private apiClient: TodoItemsApiClient) { }

  ngOnInit() {
  }

  getAll() {
    return this.apiClient.items;
  }

  addTodoItem(item: TodoItem): void {
    this.apiClient.add(item);
  }

  deleteSelectedItems(): void {
    this.apiClient.delete();
  }

  anySelected(): boolean {
    return this.apiClient.items.some(item => item.selected === true);
  }

  selectionChange(item: TodoItem): void {
    if (item.selected) {
      console.log('Se ha seleccionado ', item.name);
    } else {
      console.log('Se ha deseleccionado ', item.name);
    }
  }


  /*

    List: [1, 2, 3, 4, 5, 6]
    Selected: [1, 4, 2]



  */

}
