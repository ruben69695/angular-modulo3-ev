import { Component, OnInit } from '@angular/core';
import { TodoMovieItem } from './../../models/todo-movie-item.models';
import { TodoMoviesApiClient } from './../../models/todo-movies-api-client.model';
import { TodoItem } from 'src/app/models/todo-item.models';

@Component({
  selector: 'app-todo-movies-to-watch',
  templateUrl: './todo-movies-to-watch.component.html',
  styleUrls: ['./todo-movies-to-watch.component.css'],
  providers: [ TodoMoviesApiClient ]
})
export class TodoMoviesToWatchComponent implements OnInit {

  constructor(public apiClient: TodoMoviesApiClient) {
  }

  ngOnInit() { }

  onItemCreated(item: TodoItem): void {
    this.apiClient.add(new TodoMovieItem(item.name, new Date(), item.description));
  }
}
