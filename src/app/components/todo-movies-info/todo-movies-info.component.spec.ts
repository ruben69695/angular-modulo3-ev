import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TodoMoviesInfoComponent } from './todo-movies-info.component';

describe('TodoMoviesInfoComponent', () => {
  let component: TodoMoviesInfoComponent;
  let fixture: ComponentFixture<TodoMoviesInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TodoMoviesInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodoMoviesInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
