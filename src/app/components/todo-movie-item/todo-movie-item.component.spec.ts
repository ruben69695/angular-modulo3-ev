import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TodoMovieItemComponent } from './todo-movie-item.component';

describe('TodoMovieItemComponent', () => {
  let component: TodoMovieItemComponent;
  let fixture: ComponentFixture<TodoMovieItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TodoMovieItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodoMovieItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
