import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TodoMoviesComponent } from './todo-movies.component';

describe('TodoMoviesComponent', () => {
  let component: TodoMoviesComponent;
  let fixture: ComponentFixture<TodoMoviesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TodoMoviesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodoMoviesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
