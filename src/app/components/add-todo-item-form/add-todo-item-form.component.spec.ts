import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTodoItemFormComponent } from './add-todo-item-form.component';

describe('AddTodoItemFormComponent', () => {
  let component: AddTodoItemFormComponent;
  let fixture: ComponentFixture<AddTodoItemFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddTodoItemFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTodoItemFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
