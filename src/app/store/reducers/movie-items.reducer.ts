import { TodoMovieItem } from 'src/app/models/todo-movie-item.models';
import { TodoMoviesActions, TodoMoviesActionTypes, VoteUpTodoMovieAction,
    VoteDownTodoMovieAction,
    InitDataTodoMovieAction,
    AddTodoMovieAction} from '../actions/movies-items.actions';

// STATES
export interface TodoMoviesState {
    items: TodoMovieItem[];
}

const initializeTodoMoviesState: TodoMoviesState = {
    items: []
};

// REDUCERS
export function reducerTodoMovies(state: TodoMoviesState = initializeTodoMoviesState, action: TodoMoviesActions): TodoMoviesState {
    switch (action.type) {
        case TodoMoviesActionTypes.INIT_DATA:
            const destinos: TodoMovieItem[] = (action as InitDataTodoMovieAction).movies;
            return {
                ...state,
                items: destinos
            };
        case TodoMoviesActionTypes.VOTE_UP:
            (action as VoteUpTodoMovieAction).movieItem.voteUp();
            return {
                ...state
            };
        case TodoMoviesActionTypes.VOTE_DOWN:
            (action as VoteDownTodoMovieAction).movieItem.voteDown();
            return {
                ...state
            };
        case TodoMoviesActionTypes.ADD_MOVIE:
            return {
                ...state,
                items: [...state.items, (action as AddTodoMovieAction).movieItem]
            };
    }
    return state;
}


