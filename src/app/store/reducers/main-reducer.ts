import { ActionReducerMap } from '@ngrx/store';
import { TodoMoviesState, reducerTodoMovies } from './movie-items.reducer';
import { TodoItemsState, reducerTodoItems } from './todo-items.reducer';

export interface AppState {
    movies: TodoMoviesState;
    todoItems: TodoItemsState;
}

export const reducers: ActionReducerMap<AppState> = {
    movies: reducerTodoMovies,
    todoItems: reducerTodoItems
};
