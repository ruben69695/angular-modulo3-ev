import { InjectionToken } from '@angular/core';

export interface AppConfig {
    endPoint: string;
}

export const APP_CONFIG_VALUE: AppConfig = {
    endPoint: 'http://localhost:3000'
};

export const APP_CONFIG = new InjectionToken<AppConfig>('app.config');

